azure-terraform-modules

# Introduction 
Azure terraform module to create an Azure virtual network.


# Getting Started
TODO: Guide users through getting your code up and running on their own system. In this section you can talk about:
1.	Installation process
2.	Software dependencies
3.	Latest releases
4.	API references

# Provider
| Name    | Version |
| :---:   | :---:   |
| azurerm | >=1.35.0|

# Dependencies
This module depends on a new or existing resource group (refer to resource group module)

# Usage
Reference the module as below and pass required and/or optional arguments to provision a resource group. This will create a single module.

```
module "vnet" {
  source = "git@ssh.dev.azure.com:v3/Delu2011/az-terraform-module/az-virtual-network"
  vnet_name             = var.vnet_name
  resource_group_name   = module.vnet_resource_group.resource_group_name
  location              = module.vnet_resource_group.resource_group_location
  vnet_cidr             = var.vnet_cidr
  dns_servers           = var.dns_servers
  tags                  = var.default_tags
}
```

# Inputs
| Name | Description | Type | Default | Required |
| :---: | :---: | :---: | :---: | :---: |
| vnet_name | Name of virtual network to be created | string | n/a | yes |
| location | Azure region to use | string | n/a | yes |
| environment | Environment to deploy | string | n/a | no | 
| project | Name of project | string | n/a | no | 
| resource_group_name | Resource group name | string | n/a | yes |
| vnet_cidr | Address space used by virtual network | list(string) | n/a | yes |
| dns_servers | List of DNS servers | list(string) | n/a | no |
| tags | Tags to add to resources | map(string) | n/a | no |


# Outputs
| Name | Description |
| :---: | :---: |
| virtual_network_id | Virtual network generated id |
| virtual_network_location | Virtual network location - this is a valid Azure region |
| virtual_network_name | Virtual network name |
| virtual_network_space | Virtual network space |
